#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "matrix.h"
#include "pair.h"

struct matrix_t * consMatrix(int nbLig, int nbCol){
  assert(nbLig > 0 && nbCol > 0);
  struct matrix_t * M = calloc(1,sizeof(struct matrix_t));
  assert(M);
  M->nbLig = nbLig;
  M->nbCol = nbCol;
  M->values = calloc(nbLig*nbCol, sizeof(double));
  assert(M->values);
  return M;
}

struct matrix_t * cpyMatrix(struct matrix_t * M){
  assert(M);
  struct matrix_t * N = consMatrix(M->nbLig,M->nbCol);

  for(int k = 0; k < M->nbLig*M->nbCol; k += 1){
    N->values[k] = M->values[k];
  }
  return N;
}

void freeMatrix(struct matrix_t ** ptrM){
  assert(ptrM && *ptrM);
  free((*ptrM)->values);
  free(*ptrM);
  *ptrM = NULL;
}

struct matrix_t * scanMatrix(){
  int nbLig, nbCol;
  do
  {
    printf("Nombres de lignes et de colonnes ? ");
    scanf(" %d %d", &nbLig, &nbCol);
  } while (nbLig <= 0 || nbCol <= 0);

  struct matrix_t * M = consMatrix(nbLig, nbCol);
  for(int l=0; l<nbLig; l+=1){
    for(int c=0; c<nbCol; c+=1){
      printf("M[%d,%d] = ",l,c);
      scanf(" %lf",M->values+(l*M->nbCol+c));
    }
  }
  return M;
}

void printMatrix(struct matrix_t * M, char * entete){
  assert(M);
  struct pair_t * P = consPair(0,0);

  printf("%s\n", entete);
  for(int l = 0; l < M->nbLig; l += 1){
    P->l = l;
    printf("\t");
    for(int c = 0; c < M->nbCol; c += 1){
      P->c = c;
      printf("%'.2lf\t", M->values[pair2ind(P,M)]);
    }
    printf("\n");
  }
}

struct matrix_t * loadMatrix(char * filename, enum mode_t mode){
  FILE *fd;
  int nbLig, nbCol;
  struct matrix_t *M;
  if(mode == TXT){
    fd = fopen(filename, "rt");
    if(!fd){
      char S[40];
      sprintf(S, "Cannot open file %s\n", filename);
      perror(S);
      exit(-1);
    }
    fscanf(fd, "%d %d", &nbLig, &nbCol);
    M = consMatrix(nbLig, nbCol);
    for(int k = 0; k < nbLig*nbCol; k += 1){
      fscanf(fd, "%lf", &(M->values[k]));
    }
  }else{
    fd = fopen(filename, "rb");
    if(!fd){
      char S[40];
      sprintf(S, "Cannot open file %s\n", filename);
      perror(S);
      exit(-1);
    }
    fread(&nbLig, sizeof(double), 1, fd);
    fread(&nbCol, sizeof(double), 1, fd);
    M = consMatrix(nbLig, nbCol);
    fread(M->values, sizeof(double), M->nbLig * M->nbCol, fd);
  }
  fclose(fd);
  return M;
}

void saveMatrix(struct matrix_t *M, char * filename, enum mode_t mode){
  FILE *fd;
  if(mode == TXT) {
      fd = fopen (filename, "wt");
      if (!fd) {
        char S[40];
        sprintf(S, "Cannot open file %s\n", filename);
        perror(S);
        exit(-1);
      }
      fprintf (fd, "%d %d\n", M->nbLig, M->nbCol);
      for(int k = 0; k < M->nbLig * M->nbCol; k+=1){
        fprintf (fd, "%lf ", M->values[k]);
      }
      fprintf (fd, "\n");
  }else{
      fd = fopen(filename, "wb");
      if(!fd){
        char S[40];
        sprintf(S, "Cannot open file %s\n", filename);
        perror(S);
        exit(-1);
      }
      fwrite(&(M->nbLig), sizeof(double), 1, fd);
      fwrite(&(M->nbCol), sizeof(double), 1, fd);
      fwrite(M->values, sizeof(double), M->nbLig*M->nbCol, fd);
    }
  fclose (fd);
}

struct matrix_t * matrixAdd(struct matrix_t * A, struct matrix_t * B){
  assert(A->nbLig == B->nbLig && A->nbCol == B->nbCol);
  struct matrix_t * C = consMatrix(A->nbLig, A->nbCol);

  for ( int k = 0; k < C->nbLig*C->nbCol; k += 1 ) {
    C->values[k] = A->values[k] + B->values[k];
  }
  return C;
}

struct matrix_t * matrixMult(struct matrix_t * A, struct matrix_t * B){
  assert(A->nbCol == B->nbLig);
  struct matrix_t * C = consMatrix(A->nbLig, B->nbCol);
  struct pair_t * P = consPair ( 0, 0 );
  struct pair_t * Q = consPair ( 0, 0 );
  struct pair_t * R = consPair ( 0, 0 );
  int K = A->nbCol;

  for(int l = 0; l < C->nbLig; l += 1){
    P->l = l;
    for(int c = 0; c < C->nbCol; c += 1){
      P->c = c;
      int i = pair2ind(P, C);
      C->values[i] = 0;
      for(int k = 0; k < K; k += 1 ){
        Q->l = l, Q->c = k;
        R->l = k, R->c = c;
        C->values[i] += A->values[pair2ind(Q,A)] * B->values[pair2ind(R,B)];
      }
    }
  }
  return C;
}
