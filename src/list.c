#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "form.h"
#include "elmlist.h"
#include "list.h"

/*********************************
DÉCLARATIONS DES FONCTIONS PRIVÉES
*********************************/
void insert_after(struct list_t * L, void * datum, struct list_elm_t * ptrelm);

/**********************************
DÉFINITIONS DES FONCTIONS PUBLIQUES
**********************************/
struct list_t * new_list() {
  struct list_t * L = (struct list_t *) calloc(1, sizeof(struct list_t));
  assert(L);
  return L;
}

void del_list(struct list_t ** ptrL, void (*ptrf) ()) {
  assert(ptrL && *ptrL);
  for(struct list_elm_t * iterator = (*ptrL)->head; iterator; ) {
    // note that there is no iteration step
    struct list_elm_t * E = iterator;

    // The iteration step needs to be defined here
    iterator = iterator->suc;
    del_list_elm(&E, ptrf);
  }
  free(*ptrL);
  *ptrL = NULL;
}

bool empty_list(struct list_t * L) {
  assert(L);
  return L->numelm == 0;
}

struct list_elm_t * get_head(struct list_t * L) {
  assert(L);
  return L->head;
}

struct list_elm_t * get_tail(struct list_t * L) {
  assert(L);
  return L->tail;
}

void cons(struct list_t * L, void * datum) {
  assert(L);
  struct list_elm_t * E = new_list_elm(datum);
  E->suc = L->head;
  if( empty_list(L) ) {
    L->tail = E;
  } else {
    L->head->pred = E;
  }
  L->head = E;
  L->numelm += 1;
}

void queue(struct list_t * L, void * datum) {
  assert(L);
  struct list_elm_t * E = new_list_elm(datum);
  E->pred = L->tail;
  if ( empty_list(L) ) {
    L->head = E;
  } else {
    L->tail->suc = E;
  }
  L->tail = E;
  L->numelm += 1;
}

void insert_ordered(struct list_t * L, void * datum, int (*cmp_ptrf) ()) {
  if(empty_list(L) || (*cmp_ptrf)(datum, L->head->datum)<=0){
    cons(L,datum);
  } else if((*cmp_ptrf)(L->tail->datum, datum)<=0) {
    queue(L,datum);
  } else {
    struct list_elm_t * iterator = L->head->suc;
    while((*cmp_ptrf)(iterator->datum,datum)<0) {
      iterator = iterator->suc;
    }
    insert_after(L,datum,iterator->pred);
  }
}

/** Well well well view list indeed */
void view_list(struct list_t * L, void (*ptrf)()) {
  printf("\t\t====================\n");
  printf("\t\t|| View data list ||\n");
  printf("\t\t====================\n");
  if(empty_list(L)) {
    printf("[ ] //empty list\n");
  }
  else {
    struct list_elm_t * iterator = L->head;
    while(iterator) {
      view_list_elm(iterator, ptrf);
      iterator = iterator->suc;
    }
  }
  printf("\t\t====================\n\n");
}

/*******************************************
Définitions des fonctions privées du TA list
*******************************************/

void insert_after(struct list_t * L, void * datum, struct list_elm_t * place) {
  assert(L);
  if( empty_list(L) || !place){
    cons(L,datum);
  } else if( place == L->tail){
    queue(L, datum);
  } else {
    struct list_elm_t * E = new_list_elm(datum);
    E->pred = place;
    E->suc = place->suc;
    place->suc->pred = E;
    place->suc = E;
    L->numelm += 1;
  }
}
