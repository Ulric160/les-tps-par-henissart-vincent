#include <math.h>
#include "tools.h"

int intcmp(int * a, int * b){
  return (*a - *b);
}

int doublecmp(double * a, double * b){
  return ((fabs(*a -*b) < EPSILON) ? 0 : ((*a < *b) ? -1 : +1));
}