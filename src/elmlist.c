#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "elmlist.h"

struct  list_elm_t * new_list_elm(void * datum) {
  struct  list_elm_t * E = calloc(1, sizeof(struct  list_elm_t));
  assert(E);
  E->datum = datum;
  return E;
}

void del_list_elm(struct  list_elm_t ** ptrE, void (*ptrf) ()) {
  assert(ptrE && *ptrE);
  if(ptrf) (*ptrf)((*ptrE)->datum);
  free(*ptrE);
  *ptrE = NULL;
}

struct  list_elm_t * get_suc(struct  list_elm_t * E) {
  assert(E);
  return E->suc;
}

struct  list_elm_t * get_pred(struct  list_elm_t * E) {
  assert(E);
  return E->pred;
}

void * get_data(struct  list_elm_t * E) {
  assert(E);
  return E->datum;
}

void set_suc(struct  list_elm_t * E, struct  list_elm_t * S) {
  assert(E);
  E->suc = S;
}

void set_pred(struct  list_elm_t * E, struct  list_elm_t * P) {
  assert(E);
  E->pred = P;
}

void set_data(struct  list_elm_t * E, void * data) {
  assert(E);
  E->datum = data;
}

void view_list_elm(struct  list_elm_t * E, void (*ptrf)()) {
  assert(E && ptrf);
  (*ptrf)(E->datum);
}
