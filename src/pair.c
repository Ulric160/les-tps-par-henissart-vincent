#include <stdlib.h>
#include <assert.h>
#include "pair.h"

struct pair_t * consPair(int l, int c){
  struct pair_t * P = calloc(1,sizeof(struct pair_t));

  P->l = l;
  P->c = c;
  return P;
}

struct pair_t * cpyPair ( struct pair_t * P ) {
  struct pair_t * Q = calloc(1,sizeof(struct pair_t));

  Q->l = P->l;
  Q->c = P->c;
  return Q;
}

void freePair(struct pair_t ** ptrP){
  assert(ptrP && *ptrP);
  free (*ptrP);
  *ptrP = NULL;
}

int pair2ind(struct pair_t * P, struct matrix_t * M){
  return P->l * M->nbCol + P->c;
}

struct pair_t * ind2pair(int k, struct matrix_t * M){
  static struct pair_t * P = NULL;

  if( !P ) P = calloc(1,sizeof(struct pair_t));

  P->l = k / M->nbCol;
  P->c = k % M->nbCol;
  return P;
}
