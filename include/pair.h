#pragma once

#include "matrix.h"

/**
 * @brief Le TA correspondant
 * aux indices de ligne et
 * de colonne au sein d'une matrice
 */
struct pair_t {
  int l, c ;
};

/**
 * @brief Construire un élément de type
 * abstrait <struct pair_t> ayant pour valeur
 * le couple (l,c)
 */
struct pair_t * consPair ( int l, int c );

/**
 * @brief Faire une copie exacte
 * (mais distincte) de la paire
 * référencée par P
 */
struct pair_t * cpyPair ( struct pair_t * P );

/**
 * @brief Désallouer la mémoire référencée par P
 */
void freePair(struct pair_t ** ptrP);

/**
 * @brief (l,c) ───> k
 * cf. matrix.h
 */
int pair2ind ( struct pair_t * P, struct matrix_t * M );

/**
 * @brief k ───> (l,c)
 * cf. matrix.h
 */
struct pair_t * ind2pair ( int k, struct matrix_t * M );
