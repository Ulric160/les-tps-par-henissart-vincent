#pragma once

#define EPSILON 0.00000001

int intcmp(int * a, int * b);
int doublecmp(double * x, double * y);
