#pragma once

/**
Abstract type modeling a list element containing
- a form
- 2 pointers to its predecessor  and successor
*/
struct  list_elm_t {
  void * datum;
  struct  list_elm_t * suc;
  struct  list_elm_t * pred;
};

/** Create a new list element around datum */
struct  list_elm_t * new_list_elm(void * datum);

/* Delete list element and its data */
void del_list_elm(struct  list_elm_t ** ptrE, void (*ptrf) ());

/** Who's next ? */
struct  list_elm_t * get_suc(struct  list_elm_t * E);

/** Who's before ? */
struct  list_elm_t * get_pred(struct  list_elm_t * E);

/** What is the data ? */
void * get_data(struct  list_elm_t * E);

/** Set list element's successor to S */
void set_suc(struct  list_elm_t * E, struct  list_elm_t * S);

/** Set list element's predecessor to S */
void set_pred(struct  list_elm_t * E, struct  list_elm_t * P);

/** Set list element's datum to datum */
void set_data(struct  list_elm_t * E, void * datum);

void view_list_elm(struct  list_elm_t * E, void (*ptrf)());
