#pragma once

enum mode_t {TXT, BIN};

/** @brief une matrice de dimensions nbLigXnbCol
  * est modélisée par un vecteur mémoire
  * <values> de taille n*m.
  *
  * Il est donc nécessaire de définir des
  * fonctions de mise en correspondance
  * entre les indices (l,c) de ligne et de
  * colonne de la matrice et les indices k
  * du vecteur mémoire <values>
  */
struct matrix_t {
  double * values ;
  int nbLig, nbCol ;
};

/** @brief Effectuter l'allocation mémoire
  * d'une matrice de dimensions nXm
  */
struct matrix_t * consMatrix(int nbLig, int nbCol);

/** @brief Faire une copie exacte
  * (mais distincte) de la matrice
  * référencée par M
  */
struct matrix_t * cpyMatrix(struct matrix_t * M);

/** @brief Déallouer la mémoire référencée par M */
void freeMatrix(struct matrix_t ** ptrM);

/** @brief Lecture au clavier d'une matrice */
struct matrix_t * scanMatrix();

/** @brief Visualiser la matrice référencée par M */
void printMatrix(struct matrix_t * M, char * entete);

/**. @brief Chargement d'une matrice */
struct matrix_t * loadMatrix(char * filename, enum mode_t mode);

/** @brief Sauvegarde d'une matrice */
void saveMatrix(struct matrix_t *M, char * filename, enum mode_t mode);

/** @brief Renvoie une nouvelle matrice C = A+B */
struct matrix_t * matrixAdd ( struct matrix_t * A, struct matrix_t * B );

/** @brief Renvoie une nouvelle matrice C = AB */
struct matrix_t * matrixMult(struct matrix_t * A, struct matrix_t * B);
