#pragma once

#include <stdbool.h>
#include "elmlist.h"

/**
Abstract type for double-linked list modeled by
- 2 pointers pointing to the head and the tail of the list
- the number of element the list contains
*/
struct list_t {
  struct list_elm_t * head, * tail;
  int numelm;
};

/****************
Constructors & co
****************/
struct list_t * new_list();

void del_list(struct list_t ** ptrL, void (*ptrf) ());

bool empty_list(struct list_t * L);

/********************
Accessors & modifiers
********************/
struct list_elm_t * get_head(struct list_t * L);

struct list_elm_t * get_tail(struct list_t * L);

/** Add on head, add on tail, insert data after place localized by cmp_ptr */
void cons(struct list_t * L, void * datum);

void queue(struct list_t * L, void * datum);

void insert_ordered(struct list_t * L, void * datum, int (*cmp_ptrf)());

/** Display list on stdout stream */
void view_list(struct list_t * L, void (*ptrf)());
